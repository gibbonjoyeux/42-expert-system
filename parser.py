#!/usr/bin/python3

################################################################################
##[ MODULES ]###################################################################
################################################################################

import  sys;

printl_level = 0;

################################################################################
##[ FUNCTIONS ]#################################################################
################################################################################

##################################################
##[ OTHER ]#######################################
##################################################

##############################
def printl(message, mode):
    global printl_level;
    if (mode == "OPEN"):
        printl_level += 1;
    elif (mode == "CLOSE"):
        printl_level -= 1;

##############################
def error(detail):
    print("ERROR: " + detail);
    exit(1);

##################################################
##[ PARSING ]#####################################
##################################################

##############################
def get_entity(entity):
    if (type(entity) == str):
        return ({"type": "PROPERTY", "value": entity});
    return (entity);

##############################
def set_rev(tokens):
    max = len(tokens);
    i = 0;
    while (i < max):
        if (tokens[i] == "!"):
            reverse = False;
            while (i < max and tokens[i] == "!"):
                reverse = True if False else True;
                i += 1;
            if (i >= max):
                error("Trying to invert an empty entity.");
            if (type(tokens[i]) == str and tokens[i].isalpha() == False):
                error("Trying to invert an operator.");
            tokens[i] = {"type": "!", "value": get_entity(tokens[i])};
            tokens.pop(i - 1);
            max -= 1;
            i = 0;
        else:
            i += 1;
    return (tokens);

##############################
def set_ope(tokens, ope):
    max = len(tokens);
    i = 0;
    while (i < max):
        if (tokens[i] == ope):
            if (i == 0):
                error("Trying to add something which is missing");
            if (type(tokens[i - 1]) == str and tokens[i - 1].isalpha == False):
                error("Trying to add an operator.");
            if (type(tokens[i + 1]) == str and tokens[i + 1].isalpha == False):
                error("Trying to add an operator.");
            left = get_entity(tokens[i - 1]);
            right = get_entity(tokens[i + 1]);
            node = {"type": ope, "left": left, "right": right};
            tokens[i] = node;
            tokens.pop(i + 1);
            tokens.pop(i - 1);
            max -= 2;
            i = 0;
        else:
            i += 1;
    return (tokens);

##################################################
##[ MAIN ]########################################
##################################################

##############################
def parse(tokens):
    i = 0;
    max = len(tokens);
    while (i < max):
        if (type(tokens[i]) == list):
            tokens[i] = parse(tokens[i]);
        i += 1;
    tokens = set_rev(tokens);
    tokens = set_ope(tokens, "+");
    tokens = set_ope(tokens, "|");
    tokens = set_ope(tokens, "^");
    if (len(tokens) != 1):
        error("The condition is malformed");
    return (get_entity(tokens[0]));
